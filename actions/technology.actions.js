import { TECHNOLOGY_TYPES } from './../types'

export const fetchAllTechnologies = payload => async (dispatch, _, agent) => {
  dispatch({ type: TECHNOLOGY_TYPES.FETCH_TECHNOLOGIES_REQUEST })

  try {
    const { data } = await agent.get('/technology',  { params: payload })

    dispatch({ type: TECHNOLOGY_TYPES.FETCH_TECHNOLOGIES_SUCCESS, payload: data })

  } catch (error) {
    dispatch({ type: TECHNOLOGY_TYPES.FETCH_TECHNOLOGIES_ERROR })
  }
}

export const fetchTechnologyById = id => async (dispatch, _, agent) => {
  
  dispatch({ type: TECHNOLOGY_TYPES.FETCH_TECHNOLOGY_REQUEST })

  try {
    const { data } = await agent.get(`/technology/${id}`, { params: { id, count: 100 } })    
    dispatch({ type: TECHNOLOGY_TYPES.FETCH_TECHNOLOGY_SUCCESS, payload: data })

  } catch (e) {
    dispatch({ type: TECHNOLOGY_TYPES.FETCH_TECHNOLOGY_ERROR, payload: e.message })
  }
}

export const createTechnology = (payload, onSuccess, onError) => async(dispatch, _, agent) => {
  dispatch({ type: TECHNOLOGY_TYPES.CREATE_TECHNOLOGY_REQUEST })

  try{
    const { data: technology } = await agent.post('/technology', payload)
    dispatch({ type: TECHNOLOGY_TYPES.CREATE_TECHNOLOGY_SUCCESS, payload:  technology})
    if (onSuccess instanceof Function) onSuccess()
  }
  catch(e) {
    dispatch({ type: TECHNOLOGY_TYPES.CREATE_TECHNOLOGY_ERROR, payload: e.message })
    if (onError instanceof Function) onError()
  }
}

export const updateTechnology = (payload, onSuccess, onError) => async(dispatch, _, agent) => {
  dispatch({ type: TECHNOLOGY_TYPES.UPDATE_TECHNOLOGY_REQUEST })
  try{
    const { data: technology } = await agent.put(`/technology/${ payload.technologyId }`, { ...payload, id: payload.technologyId })
    dispatch({ type: TECHNOLOGY_TYPES.UPDATE_TECHNOLOGY_SUCCESS, payload: technology })
    if (onSuccess instanceof Function) onSuccess()
  }
  catch(e) {
    dispatch({ type: TECHNOLOGY_TYPES.UPDATE_TECHNOLOGY_ERROR, payload: e.message })
    if (onError instanceof Function) onError()
  }
}