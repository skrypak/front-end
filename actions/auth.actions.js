import { AUTH_TYPES, USER_TYPES } from './../types'
import Cookies from 'js-cookie'

export const login = (payload, onSuccess) => async (dispatch, _, agent) => {
  dispatch({ type: AUTH_TYPES.LOGIN_REQUEST })

  try {
    const { data: { access_token } } = await agent.post('/auth/login', payload)

    Cookies.set('token', access_token)
    dispatch({ type: AUTH_TYPES.LOGIN_SUCCESS })

    const { data: user } = await agent.get('/user/me')
    dispatch({ type: USER_TYPES.FILL_USER, payload: user })

    if (onSuccess instanceof Function) onSuccess()
  } catch (error) {
    Cookies.remove('token')
    dispatch({ type: AUTH_TYPES.LOGIN_ERROR, payload: 'Email or password is incorrect' })
  }
}

export const logout = (onSuccess) => dispatch => {
  Cookies.remove('token')
  dispatch({ type: AUTH_TYPES.LOGOUT })

  if (onSuccess instanceof Function) onSuccess()
}

export const createPassword = (payload, onSuccess) => async (dispatch, _, agent) => {
  dispatch({ type: AUTH_TYPES.CREATE_PASS_REQUEST })

  try {
    await agent.post('/user-reset-password/reset-complete', payload)
    dispatch({ type: AUTH_TYPES.CREATE_PASS_SUCCESS })
    if (onSuccess instanceof Function) onSuccess()
  } catch (err) {
    dispatch({ type: AUTH_TYPES.CREATE_PASS_ERROR, payload: err.message })
  }
}

export const resetPassword = (payload, onSuccess) => async (dispatch, _, agent) => {
  dispatch({ type: AUTH_TYPES.RESET_PASS_REQUEST })
  try {
    await agent.post('/user-reset-password/reset', payload)
    dispatch({ type: AUTH_TYPES.RESET_PASS_SUCCESS })
    if (onSuccess instanceof Function) onSuccess()
  } catch (err) {
    dispatch({ type: AUTH_TYPES.RESET_PASS_ERROR, payload: err.message })
  }
}