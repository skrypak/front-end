import { TECHNOLOGY_TYPE_TYPES } from './../types'

export const fetchAllTechnologyTypes = payload => async (dispatch, _, agent) => {
  dispatch({ type: TECHNOLOGY_TYPE_TYPES.FETCH_TECHNOLOGIES_TYPE_REQUEST })

  try {
    const { data } = await agent.get('/technology-type',  { params: payload })

    dispatch({ type: TECHNOLOGY_TYPE_TYPES.FETCH_TECHNOLOGIES_TYPE_SUCCESS, payload: data })

  } catch (error) {
    dispatch({ type: TECHNOLOGY_TYPE_TYPES.FETCH_TECHNOLOGIES_TYPE_ERROR })
  }
}

export const fetchTechnologyTypeById = id => async (dispatch, _, agent) => {
  dispatch({ type: TECHNOLOGY_TYPE_TYPES.FETCH_TECHNOLOGY_TYPE_REQUEST })

  try {
    const { data } = await agent.get(`/technology-type/${id}`, { params: { id, count: 100 } })
    dispatch({ type: TECHNOLOGY_TYPE_TYPES.FETCH_TECHNOLOGY_TYPE_SUCCESS, payload: data })

  } catch (e) {
    dispatch({ type: TECHNOLOGY_TYPE_TYPES.FETCH_TECHNOLOGY_TYPE_ERROR, payload: e.message })
  }
}

export const createTechnologyType = (payload, onSuccess, onError) => async(dispatch, _, agent) => {
  dispatch({ type: TECHNOLOGY_TYPE_TYPES.CREATE_TECHNOLOGY_TYPE_REQUEST })

  try{
    const { data: technologyType } = await agent.post('/technology-type', payload)
    dispatch({ type: TECHNOLOGY_TYPE_TYPES.CREATE_TECHNOLOGY_TYPE_SUCCESS, payload:  technologyType})
    if (onSuccess instanceof Function) onSuccess()
  }
  catch(e) {
    dispatch({ type: TECHNOLOGY_TYPE_TYPES.CREATE_TECHNOLOGY_TYPE_ERROR, payload: e.message })
    if (onError instanceof Function) onError()
  }
}

export const updateTechnologyType = (payload, onSuccess, onError) => async(dispatch, _, agent) => {
  dispatch({ type: TECHNOLOGY_TYPE_TYPES.UPDATE_TECHNOLOGY_TYPE_REQUEST })
  try{
    const { data: technologyType } = await agent.put(`/technology-type/${payload.id}`, payload)
    dispatch({ type: TECHNOLOGY_TYPE_TYPES.UPDATE_TECHNOLOGY_TYPE_SUCCESS, payload: technologyType })
    if (onSuccess instanceof Function) onSuccess()
  }
  catch(e) {
    dispatch({ type: TECHNOLOGY_TYPE_TYPES.UPDATE_TECHNOLOGY_TYPE_ERROR, payload: e.message })
    if (onError instanceof Function) onError()
  }
}