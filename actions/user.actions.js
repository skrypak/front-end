import { USER_TYPES } from './../types'

export const fillUser = payload => ({ type: USER_TYPES.FILL_USER, payload })

export const filterUsers = payload => async (dispatch, _, agent) => {
  dispatch({ type: USER_TYPES.FILTER_USERS_REQUEST })

  try {
    const { data: users } = await agent.get('/user', { params: payload })
    dispatch({ type: USER_TYPES.FILTER_USERS_SUCCESS, payload: users })
  } catch (e) {
    dispatch({ type: USER_TYPES.FILTER_USERS_ERROR })
  }
}

export const fetchAllUsers = () => async (dispatch, _, agent) => {
  dispatch({ type: USER_TYPES.FETCH_ALL_USERS_REQUEST })

  try {
    const { data: data } = await agent.get('/user/all')

    dispatch({ type: USER_TYPES.FETCH_ALL_USERS_SUCCESS, payload: data })
  } catch (e) {
    dispatch({ type: USER_TYPES.FETCH_ALL_USERS_ERROR, payload: e })
  }
}

export const fetchByPositionId = id => async (dispatch, _, agent) => {
  dispatch({ type: USER_TYPES.FETCH_USERS_BY_POSITION_REQUEST })

  try {
    const { data: { data } } = await agent.get(`/user`, { params: { position: id, count: 100 } })
    dispatch({ type: USER_TYPES.FETCH_USERS_BY_POSITION_SUCCESS, payload: data })
  } catch (e) {
    dispatch({ type: USER_TYPES.FETCH_USERS_BY_POSITION_ERROR, payload: e })
  }
}

export const createUser = (payload, onSuccess, onError) => async (dispatch, _, agent) => {
  dispatch({ type: USER_TYPES.CREATE_USER_REQUEST })

  try {
    await agent.post('/user', payload)

    dispatch({ type: USER_TYPES.CREATE_USER_SUCCESS })
    if (onSuccess instanceof Function) onSuccess()
  } catch (e) {
    dispatch({ type: USER_TYPES.CREATE_USER_ERROR, payload: e.message })
    if (onError instanceof Function) onError()
  }
}

export const updateUser = (id, payload, onSuccess) => async (dispatch, _, agent) => {
  dispatch({ type: USER_TYPES.UPDATE_USER_REQUEST })
  try {
    await agent.put(`/user/${ id }`, payload)

    dispatch({ type: USER_TYPES.UPDATE_USER_SUCCESS })
    if (onSuccess instanceof Function) onSuccess()
  } catch (e) {
    dispatch({ type: USER_TYPES.UPDATE_USER_ERROR })
  }
}

export const fetchUserById = (id, onSuccess) => async (dispatch, _, agent) => {
  dispatch({ type: USER_TYPES.FETCH_USER_REQUEST })
  try {
    const { data: user } = await agent.get(`/user/${ id }`)

    dispatch({ type: USER_TYPES.FETCH_USER_SUCCESS, payload: user })
    if (onSuccess instanceof Function) onSuccess()
  } catch (e) {
    dispatch({ type: USER_TYPES.FETCH_USER_ERROR })
  }
}

export const clearManagedUser = () => ({ type: USER_TYPES.CLEAR_MANAGED_USER })
