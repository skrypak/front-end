import React from 'react'
import { parseCookies } from 'nookies'

import { Page } from './../decorators'
import { LoginContainer } from './../containers'
import { CommonUtils } from './../utils'

@Page({ sidebar: false, header: false })
export default class Index extends React.Component {
  static async getInitialProps(ctx) {
    const { token } = parseCookies(ctx)
    if (token) CommonUtils.redirectTo('/dashboard', ctx.res)
    return {}
  }

  render() {
    return <LoginContainer />
  }
}
