import React from 'react'
import { Page, Access } from '../decorators/index'
import { ROLES } from '../utils/config'
import { ListOfIdp } from '../containers/list-of-idp/list-of-idp.container'

@Page({ sidebar: true, header: true, auth: true })
@Access([ ROLES.ROOT, ROLES.ADMIN ])
export default class IDP extends React.Component {
  render() {
    return <ListOfIdp />
  }
}
