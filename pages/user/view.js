import React from 'react'
import { Page, Access } from './../../decorators'
import { ROLES } from './../../utils/config'
import { ViewUserContainer } from '../../containers'

@Page({ sidebar: true, header: true, auth: true })
@Access([ ROLES.ROOT, ROLES.ADMIN ])
export default class Users extends React.Component {
  static getInitialProps({ query }) {
    return { userId: query.userId }
  }

  render() {
    return <ViewUserContainer userId={ this.props.userId } />
  }
}
