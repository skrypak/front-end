import React from 'react'
import { Page, Access } from './../../decorators'
import { ROLES } from './../../utils/config'
import { ManageUserContainer } from './../../containers'

@Page({ sidebar: true, header: true, auth: true })
@Access([ ROLES.ROOT, ROLES.ADMIN ])
export default class EditUser extends React.Component {
  static getInitialProps({ query }) {
    return { userId: query.userId }
  }

  render() {
    return <ManageUserContainer edit={ true } userId={ this.props.userId } />
  }
}
