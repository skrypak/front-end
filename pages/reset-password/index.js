import React from 'react'
import { parseCookies } from 'nookies'
import { CommonUtils } from './../../utils'
import { Page } from './../../decorators'
import { ResetPasswordContainer } from '../../containers'

@Page({ sidebar: false, header: false })
export default class CreatePassword extends React.Component {
  static getInitialProps(ctx) {
    const { query,res } = ctx;

    const { token } = parseCookies(ctx)
    if (token) CommonUtils.redirectTo('/idp', res)

    if (!query.token) CommonUtils.redirectTo('/reset-password/reset', res)
    return { token: query.token }
  }
  render() {
    return <ResetPasswordContainer token={ this.props.token } />
  }
}
