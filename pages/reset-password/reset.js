import React from 'react'
import { CommonUtils } from './../../utils'
import { parseCookies } from 'nookies'
import { Page } from './../../decorators'
import { ResetPasswordContainer } from '../../containers'

@Page({ sidebar: false, header: false })
export default class ResetPassword extends React.Component {
  static getInitialProps(ctx) {
    const { token } = parseCookies(ctx)
    if (token) CommonUtils.redirectTo('/idp', ctx.res)
  }
  
  render() {
    return <ResetPasswordContainer IsResetPassword={true} />
  }
}
