import React from 'react'
import { Page, Access } from './../../decorators'
import { ROLES } from './../../utils/config'
import { AdminDashboard, UserDashboard } from '../../containers'

@Page({ sidebar: true, header: true, auth: true })
@Access([ROLES.ALL])
export default class Users extends React.Component {
  render() {
    switch (this.props.role.name) {
      case ROLES.ADMIN:
      case ROLES.ROOT:
        return <AdminDashboard />
      case ROLES.USER:
        return <UserDashboard />
    }
  }
}
