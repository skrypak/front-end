import React from 'react'
import { Page, Access } from './../../decorators'
import { ROLES } from './../../utils/config'
import { ManageTechnologyTypeContainer } from './../../containers'

@Page({ sidebar: true, header: true, auth: true })
@Access([ ROLES.ROOT, ROLES.ADMIN ])
export default class EditUser extends React.Component {
  static getInitialProps({ query }) {
    return { technologyTypeId: query.technologyTypeId }
  }

  render() {    
    return <ManageTechnologyTypeContainer edit={ true } technologyTypeId={ this.props.technologyTypeId } />    
  }
}
