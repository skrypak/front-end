import React from 'react'
import { Page, Access } from './../../decorators'
import { ROLES } from './../../utils/config'
import { ManageProjectContainer } from '../../containers'

@Page({ sidebar: true, header: true, auth: true })
@Access([ ROLES.ROOT, ROLES.ADMIN ])
export default class CreateProject extends React.Component {
  render() {
    return <ManageProjectContainer />
  }
}
