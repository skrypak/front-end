import React from 'react'
import { Page, Access } from './../../decorators'
import { ROLES } from './../../utils/config'
import { ViewProjectContainer } from '../../containers'

@Page({ sidebar: true, header: true, auth: true })
@Access([ ROLES.ROOT, ROLES.ADMIN ])
export default class ViewProject extends React.Component {
  static getInitialProps({ query }) {
    return { projectId: query.projectId }
  }

  render() {
    return <ViewProjectContainer projectId={ this.props.projectId } />
  }
}
