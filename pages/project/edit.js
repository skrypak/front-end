import React from 'react'
import { Page, Access } from './../../decorators'
import { ROLES } from './../../utils/config'
import { ManageProjectContainer } from '../../containers'

@Page({ sidebar: true, header: true, auth: true })
@Access([ ROLES.ROOT, ROLES.ADMIN ])
export default class EditProject extends React.Component {
  static getInitialProps({ query }) {
    return { projectId: query.projectId }
  }
  render() {
    return <ManageProjectContainer edit={ true } projectId={ this.props.projectId } />
  }
}
