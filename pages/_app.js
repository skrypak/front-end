import App, { Container } from 'next/app'
import Head from 'next/head'
import React from 'react'
import { Provider } from 'react-redux'
import withRedux from 'next-redux-wrapper'

import { configureStore } from './../utils'

@withRedux(configureStore)
export default class MyApp extends App {
  static async getInitialProps({ Component, router, ctx }) {
    let pageProps = {}

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    return { pageProps }
  }

  render() {
    const { Component, pageProps, store } = this.props
    return (
      <Container>
        <Head>
          <title>Tech Review App</title>
        </Head>
        <Provider store={ store }>
          <Component { ...pageProps } />
        </Provider>
      </Container>
    )
  }
}
