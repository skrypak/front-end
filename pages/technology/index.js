import React from 'react'
import { Access, Page } from '../../decorators'
import { ROLES } from '../../utils/config'
import { ListOfTechnology } from '../../containers'

@Page({ sidebar: true, header: true, auth: true })
@Access([ROLES.ROOT, ROLES.ADMIN])
export default class DepartmentPage extends React.Component {
  static getInitialProps({ query }) {
    return { technologyId: query.technologyId }
  }

  render() {
    return <ListOfTechnology technologyId={ this.props.technologyId } />;
  }
}