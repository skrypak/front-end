import React from 'react'
import { Page, Access } from './../../decorators'
import { ROLES } from './../../utils/config'
import { ManageTechnologyContainer } from './../../containers'

@Page({ sidebar: true, header: true, auth: true })
@Access([ ROLES.ROOT, ROLES.ADMIN ])
export default class EditUser extends React.Component {
  static getInitialProps({ query }) {
    return { technologyId: query.technologyId }
  }

  render() {    
    return <ManageTechnologyContainer edit={ true } technologyId={ this.props.technologyId } />    
  }
}
