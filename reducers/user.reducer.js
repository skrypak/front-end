import { USER_TYPES, COMMON_TYPES } from './../types'

const initialState = {
  currentUser: {},
  users: [],
  usersByPosition: [],
  managedUser: {},
  processing: false,
  count: 0,
  page: 0,
  total: 0,
}

export const UserReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case USER_TYPES.FILL_USER:
      return { ...state, currentUser: { ...payload } }
    case USER_TYPES.FILTER_USERS_REQUEST:
      return { ...state, processing: true }
    case USER_TYPES.FETCH_ALL_USERS_REQUEST:
      return { ...state, processing: true }
    case USER_TYPES.FILTER_USERS_SUCCESS:
      return {
        ...state,
        users: [ ...payload.data.map(user => {
          user.key = user.id
          return user
        }) ],
        count: payload.count,
        page: payload.page,
        total: payload.total,
        processing: false,
      }
    case USER_TYPES.FETCH_ALL_USERS_SUCCESS:
      return { ...state, users: [ ...payload ] }
    case USER_TYPES.FETCH_USERS_BY_POSITION_SUCCESS:
      return { ...state, usersByPosition: [ ...payload ] }
    case USER_TYPES.FETCH_ALL_USERS_ERROR:
      return { ...state, processing: false }
    case USER_TYPES.FILTER_USERS_ERROR:
      return { ...state, processing: false }
    case USER_TYPES.FETCH_USER_SUCCESS:
    case USER_TYPES.UPDATE_USER_SUCCESS:
      return { ...state, managedUser: { ...payload } }
    case USER_TYPES.CLEAR_MANAGED_USER:
      return { ...state, managedUser: {} }
    case COMMON_TYPES.CLEAR_STORE:
      return { ...initialState }
    default:
      return state
  }
}
