import { AUTH_TYPES } from './../types'

const initialState = {
  error: null,
  loading: false
}

export const AuthReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case AUTH_TYPES.LOGIN_REQUEST:
    case AUTH_TYPES.RESET_PASS_REQUEST:
    case AUTH_TYPES.CREATE_PASS_REQUEST:
      return { ...state, error: null, loading: true }
    case AUTH_TYPES.LOGIN_ERROR:
    case AUTH_TYPES.RESET_PASS_ERROR:
    case AUTH_TYPES.CREATE_PASS_ERROR:
      return { ...state, error: payload, loading: false }
    case AUTH_TYPES.LOGIN_SUCCESS:
    case AUTH_TYPES.RESET_PASS_SUCCESS:
    case AUTH_TYPES.CREATE_PASS_SUCCESS:
      return { ...state, loading: false }
    default:
      return state
  }
}
