import { TECHNOLOGY_TYPES } from './../types'

const initialState = {
  technologies: [],
  technology: {},
  loading: true,
  error: false, 
}

export const TechnologyReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case TECHNOLOGY_TYPES.FETCH_TECHNOLOGIES_REQUEST:
    case TECHNOLOGY_TYPES.FETCH_TECHNOLOGY_REQUEST:
    case TECHNOLOGY_TYPES.CREATE_TECHNOLOGY_REQUEST:
    case TECHNOLOGY_TYPES.UPDATE_TECHNOLOGY_REQUEST:
      return { ...state, error: false, loading: true }
    case TECHNOLOGY_TYPES.FETCH_TECHNOLOGIES_ERROR:    
      return { ...state, error: true, loading: false }
    case TECHNOLOGY_TYPES.FETCH_TECHNOLOGIES_SUCCESS:
      return { ...state, loading: false, technologies: [ ...payload ] }
    case TECHNOLOGY_TYPES.FETCH_TECHNOLOGY_SUCCESS:
      return { ...state, technology: { ...payload }, loading: false }
    case TECHNOLOGY_TYPES.CREATE_TECHNOLOGY_SUCCESS:
      return { ...state, technologies: [...state.technologies, payload], loading: false }
    case TECHNOLOGY_TYPES.UPDATE_TECHNOLOGY_SUCCESS:
      return { ...state, loading: false, technologies: state.technologies.map(t => {
          if (t.id === payload.id) t = { ...payload };
          return t;
        }) };
    default:
      return state
  }
}
