import { TECHNOLOGY_TYPE_TYPES } from './../types'

const initialState = {
  technologyTypes: [],
  technologyType: {},
  loading: true,
  error: false,
}

export const TechnologyTypeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case TECHNOLOGY_TYPE_TYPES.FETCH_TECHNOLOGIES_TYPE_REQUEST:
    case TECHNOLOGY_TYPE_TYPES.FETCH_TECHNOLOGY_TYPE_REQUEST:
    case TECHNOLOGY_TYPE_TYPES.CREATE_TECHNOLOGY_TYPE_REQUEST:
    case TECHNOLOGY_TYPE_TYPES.UPDATE_TECHNOLOGY_TYPE_REQUEST:
      return { ...state, error: false, loading: true }
    case TECHNOLOGY_TYPE_TYPES.FETCH_TECHNOLOGIES_TYPE_ERROR:
      return { ...state, error: true, loading: false }
    case TECHNOLOGY_TYPE_TYPES.UPDATE_TECHNOLOGY_TYPE_ERROR:
      return { ...state, error: true, loading: false }
    case TECHNOLOGY_TYPE_TYPES.FETCH_TECHNOLOGIES_TYPE_SUCCESS:
      return { ...state, loading: false, technologyTypes: [ ...payload ] }
    case TECHNOLOGY_TYPE_TYPES.CREATE_TECHNOLOGY_TYPE_SUCCESS:
      return { ...state, technologyTypes: [...state.technologyTypes, payload], loading: false }
    case TECHNOLOGY_TYPE_TYPES.UPDATE_TECHNOLOGY_TYPE_SUCCESS:
      return {
        ...state,
        loading: false,
        technologyTypes: state.technologyTypes.map(t => {
          if (t.id === payload.id) t = { ...payload }
          return t
        }),
      }
    case TECHNOLOGY_TYPE_TYPES.FETCH_TECHNOLOGY_TYPE_SUCCESS:
      return { ...state, technologyType: { ...payload }, loading: false }
    default:
      return state
  }
}
