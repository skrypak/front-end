import { combineReducers } from 'redux'
import { AuthReducer } from './auth.reducer'
import { CommonReducer } from './common.reducer'
import { UserReducer } from './user.reducer'
import { IdpReducer } from './idp.reducer'
import { CityReducer } from './city.reducer'
import { PositionReducer } from './position.reducer'
import { DepartmentReducer } from './department.reducer'
import { TechnologyTypeReducer } from './technology-type.reducer'
import { TechnologyReducer } from './technology.reducer'
import { ClientReducer } from './client.reducer'
import { ClientStatusReducer } from './client-status.reducer'
import { ProjectTypeReducer } from './project-type.reducer'
import { ProjectStatusReducer } from './project-status.reducer'
import { ProjectToolReducer } from './project-tool.reducer'
import { ProjectUserRolesReducer } from './project-user-roles.reducer'
import { ProjectReducer } from './project.reducer'
import { EnglishLevelReducer } from './english-level.reducer'
import { userRolesReducer } from './user-roles.reducer'
import { ConfigurationReducer } from './configuration.reducer'

const rootReducer = combineReducers({
  auth: AuthReducer,
  common: CommonReducer,
  user: UserReducer,
  idp: IdpReducer,
  city: CityReducer,
  position: PositionReducer,
  department: DepartmentReducer,
  technologyType: TechnologyTypeReducer,
  technology: TechnologyReducer,
  client: ClientReducer,
  clientStatus: ClientStatusReducer,
  projectType: ProjectTypeReducer,
  projectStatus: ProjectStatusReducer,
  projectTool: ProjectToolReducer,
  projectUserRoles: ProjectUserRolesReducer,
  project: ProjectReducer,
  configuration: ConfigurationReducer,
  englishLevel: EnglishLevelReducer,
  userRoles: userRolesReducer,
})

export default rootReducer
