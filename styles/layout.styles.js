import styled from 'styled-components'
import { Layout as L } from 'antd'

export const Layout = styled(L.Content)`
  margin: 24px;
`
