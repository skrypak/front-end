import * as AuthPageStyles from './auth-page.styles'
import * as LayoutStyles from './layout.styles'

export { AuthPageStyles, LayoutStyles }
