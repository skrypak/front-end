FROM node:10

WORKDIR /var/www/tech-review/
ADD package.json /var/www/tech-review
ADD yarn.lock /var/www/tech-review
RUN yarn install

ADD . /var/www/tech-review

RUN npm run build

EXPOSE 3001

CMD npm start
