import PropTypes from 'prop-types'

export const departmentShape = {
  name: PropTypes.string,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ])
}

export const positionShape = {
  name: PropTypes.string,
  id: PropTypes.oneOfType([     PropTypes.string,     PropTypes.number   ]),
}

export const cityShape = {
  name: PropTypes.string.isRequired,
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
}

export const technologyShape = {
  name: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  type: PropTypes.string,
}

export const socialShape = {
  url: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
}

export const userShape = {
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
  first_name: PropTypes.string.isRequired, 
  last_name: PropTypes.string.isRequired,
  email: PropTypes.string,
  phone: PropTypes.string,
  gender: PropTypes.string,
  date_start_job: PropTypes.string,
  date_end_job: PropTypes.string,
  city: PropTypes.shape(cityShape),
  technologies: PropTypes.arrayOf(
    PropTypes.shape(technologyShape)
  ),
  social: PropTypes.arrayOf(
    PropTypes.shape(socialShape)
  ),
  position: PropTypes.shape(positionShape),
  department:  PropTypes.shape(departmentShape),
}
