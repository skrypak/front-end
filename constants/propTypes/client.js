import PropTypes from 'prop-types'
import { projectShape } from './project'

export const clientStatusShape = {
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]).isRequired,
  name: PropTypes.string.isRequired,
}

export const saleManagerShape = {
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]).isRequired,
  first_name: PropTypes.string.isRequired,
  last_name: PropTypes.string.isRequired,
}

export const clientShape = {
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]).isRequired,
  first_name: PropTypes.string.isRequired,
  last_name: PropTypes.string.isRequired,
  email: PropTypes.string,
  phone: PropTypes.string,
  loading: PropTypes.bool,
  country: PropTypes.string,
  timezone: PropTypes.string,
  contactPerson: PropTypes.string,
  status: PropTypes.shape(clientStatusShape),
  saleManager: PropTypes.shape(saleManagerShape),
  projects: PropTypes.arrayOf(PropTypes.shape(projectShape))
}
