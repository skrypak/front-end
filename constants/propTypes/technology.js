import PropTypes from 'prop-types'

export const technologyShape = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  projectid: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
  type: PropTypes.string,
  access: PropTypes.string,
}
