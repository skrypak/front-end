import PropTypes from 'prop-types'
import { userShape } from './user'
import { clientShape } from './client'
import { technologyShape } from './technology'

export const projectStatusShape = {
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]).isRequired,
  name: PropTypes.string.isRequired,
}

export const projectShape = {
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]).isRequired,
  name: PropTypes.string.isRequired,
  type: PropTypes.string,
  image: PropTypes.string,
  payment: PropTypes.string,
  estimate: PropTypes.number,
  cooperation: PropTypes.string,
  contactPerson: PropTypes.string,
  date_start_job: PropTypes.string,
  date_end_job: PropTypes.string,
  saleManager: PropTypes.shape(userShape),
  status: PropTypes.shape(projectStatusShape),
  tools: PropTypes.arrayOf(
    PropTypes.shape(technologyShape)
  ),
  usersRef: PropTypes.arrayOf( 
    PropTypes.shape(userShape),                                               
  ),
  client: PropTypes.oneOfType([
    PropTypes.shape(userShape), // joined client
    PropTypes.number            // or client id
  ]),
}
