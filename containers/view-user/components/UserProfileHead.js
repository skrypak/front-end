import React from 'react'
import PropTypes from 'prop-types'
import { userShape } from '../../../constants/propTypes/user'
import { Avatar } from 'antd'
import { TextCenter, AvatarCaption, AvatarSubcaption } from '../../../components/common'
import { fullNameOf, getDate } from '../../../utils/helpers/stringFormatting'

const DATE_FORMAT = 'MMM YYYY'
const getDepartmentName = department => (department || {}).name || 'no department'
const getPositionName = position => (position || {}).name || 'no position'

const UserProfileHead = ({user}) => (
  <TextCenter>
    <Avatar icon='user' src={user.photo} size={100}/>
    <AvatarCaption> {fullNameOf(user)}</AvatarCaption>
    <AvatarSubcaption>
      { getDepartmentName(user.department) } 
      { ', ' }
      { getPositionName(user.position) }

      {
        user.date_start_job && ( 
          <p>
            { getDate(user.date_start_job, { format: DATE_FORMAT }) }
            { ' - ' }
            { getDate(user.date_end_job, { format: DATE_FORMAT, noDate: 'present' }) }
          </p>
        )
      }
    </AvatarSubcaption>
  </TextCenter>
)

UserProfileHead.propTypes = {
  user: PropTypes.shape(userShape)
}

export default UserProfileHead
