import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Card, Col, Row, Skeleton, Divider, Tabs, Table } from 'antd'
import { UserActions } from './../../actions'
import { getDate } from '../../utils/helpers/stringFormatting'
import Router from 'next/router'
import Link from 'next/link'
import UserMainInfo  from '../../components/UserMainInfo'
import UserProfileHead from './components/UserProfileHead'
import SocialsList from '../../components/SocialsList'
import TechologiesTagsList from '../../components/TechologiesTagsList'

const { fetchUserById, clearManagedUser, filterUsers } = UserActions
const TabPane = Tabs.TabPane

@connect(
  state => ({
    user: state.user.managedUser,
  }),
  dispatch => ({
    actions: bindActionCreators({ fetchUserById, clearManagedUser, filterUsers }, dispatch),
  }),
)
export class ViewUserContainer extends React.Component {
  static propTypes = {
    userId: PropTypes.oneOfType([ PropTypes.number, PropTypes.string ]).isRequired,
    actions: PropTypes.shape({
      fetchUserById: PropTypes.func.isRequired,
      clearManagedUser: PropTypes.func.isRequired,
      filterUsers: PropTypes.func.isRequired,
    }),
  }

  state = { 
    loading: false
  }

  componentDidMount() {
    this.props.actions.fetchUserById(this.props.userId, () => this.setState({ loading: false }))
  }

  componentWillUnmount() {
    this.props.actions.clearManagedUser()
  }

  onUpdateClick = () => Router.push(`/user/edit?userId=${this.props.userId}`)

  generateProjectsColumns = () => ([
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      width: 150,
      render: (text, { project }) => <Link
        href={ `/project/view?projectId=${ project.id }` }><a>{ project.name }</a></Link>,
    },
    {
      title: 'Involvement',
      dataIndex: 'involvement',
      key: 'involvement',
      width: 80,
      render: (text, record) => `${ record.involvement } / 8`,
    },
    {
      title: 'Start work',
      dataIndex: 'start',
      key: 'start',
      width: 80,
      render: (text, record) => getDate(record.start, { noDate: 'In planning'})
    },
    {
      title: 'End work',
      dataIndex: 'end',
      key: 'end',
      width: 80,
      render: (text, record) => getDate(record.end),
    },
  ])

  render() {
    const { user } = this.props
    const { email, phone, city, technologies, social} = user

    return (
      <Row gutter={24}>
        <Col span={5}>
          <Card >
            <Skeleton active paragraph={ { rows: 5 } } loading={ this.state.loading }>
              <UserProfileHead user={user}/>
              <UserMainInfo 
                email={email} 
                phone={phone} 
                locationString={(city || {}).name || 'none'}
               />
              <Divider />
              <TechologiesTagsList technologies={technologies} />
              <SocialsList socials={social} />
            </Skeleton >
          </Card>
        </Col>
        <Col span={19}>
          <Card>
            <Tabs defaultActiveKey="1">
              <TabPane tab={ 'Projects' } key={ '1' }>
              <Skeleton active paragraph={ { rows: 3 } } loading={ this.state.loading }>
                <Table
                    size={ 'medium' }
                    showHeader={ true }
                    rowKey="id"
                    pagination={ false }
                    bordered={ false }
                    hasData={ (user.projectsRef || {}).length > 0 }
                    columns={ this.generateProjectsColumns() }
                    dataSource={ user.projectsRef }
                  />
              </Skeleton>
              </TabPane>
            </Tabs>
          </Card>
        </Col>
      </Row>
    )
  }
}
