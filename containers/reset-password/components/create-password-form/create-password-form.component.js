import React from 'react'
import { Button, Card, Form, Input, Icon, Row, Col } from 'antd'

const FormItem = Form.Item

@Form.create({})
export class CreatePasswordForm extends React.Component {
  handleSubmit = event => {
    event.preventDefault()
    this.props.form.validateFieldsAndScroll((err, { password, repeatPassword }) => {
      if (err) return

      if (password !== repeatPassword) {
        this.props.form.setFields({
          repeatPassword: {
            value: repeatPassword,
            errors: [new Error(`Passwords don't match`)]
          }
        })
      } else {
        this.props.onSubmit({ password, repeatPassword })
      }
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form
    const { loading } = this.props

    return (
      <div>
        <Row type="flex" justify="center" align="middle" style={{ height: '100vh' }}>
          <Col md={ 8 }>
            <Form onSubmit={ this.handleSubmit }>
              <Card title="Reset Password">
                <Row>
                  <FormItem label="New password">
                    { getFieldDecorator('password', {
                      rules: [ { required: true, message: 'Please input new password' } ]
                    })(
                      <Input
                        prefix={ <Icon type="lock" style={ { color: 'rgba(0,0,0,.25)' } } /> }
                        type="password"
                        placeholder="Type new password"
                        disabled={ loading }
                      />
                    ) }
                  </FormItem>
                </Row>
                <Row>
                  <FormItem label="Repeat new password">
                    { getFieldDecorator('repeatPassword', {
                      rules: [ { required: true, message: 'Please repeat new password' } ]
                    })(
                      <Input
                        prefix={ <Icon type="lock" style={ { color: 'rgba(0,0,0,.25)' } } /> }
                        type="password"
                        placeholder="Confirm password"
                        disabled={ loading }
                      />
                    )}
                  </FormItem>
                </Row>
                <Button type="primary" htmlType="submit" disabled={ loading }>
                  Submit
                </Button>
              </Card>
            </Form>
          </Col>
        </Row>
      </div>
    )
  }
}
