import React from 'react'
import { Button, Card, Form, Input, Icon, Row, Col } from 'antd'

const FormItem = Form.Item

@Form.create({})
export class ResetPasswordForm extends React.Component {
  handleSubmit = event => {
    event.preventDefault()
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) return

      this.props.onSubmit(values)
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form
    const { loading } = this.props

    return (
      <div>
        <Row type="flex" justify="center" align="middle" style={{ height: '100vh' }}>
          <Col md={ 8 }>
            <Form onSubmit={ this.handleSubmit }>
              <Card>
                <Row>
                  <FormItem label="Email">
                    { getFieldDecorator('email', {
                      rules: [{ required: true, message: 'Please input your email' }]
                    })(
                      <Input
                        prefix={ <Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} /> }
                        type="email"
                        placeholder="Type your email"
                        disabled={ loading }
                      />
                    )}
                  </FormItem>
                </Row>
                <Button type="primary" htmlType="submit" disabled={ loading }>
                  Submit
                </Button>
              </Card>
            </Form>
          </Col>
        </Row>
      </div>
    )
  }
}
