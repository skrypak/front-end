import React from 'react'
import Router from 'next/router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ResetPasswordForm, CreatePasswordForm } from './components'
import { AuthActions } from './../../actions'

const { resetPassword, createPassword } = AuthActions

@connect(
  state => ({ loading: state.auth.loading }),
  dispatch => ({ actions: bindActionCreators({ resetPassword, createPassword }, dispatch) })
)
export class ResetPasswordContainer extends React.Component {

  handleCreatePassSubmit = values => {
    const { token: reset_token } = this.props
    const { password: password_hash } = values
    this.props.actions.createPassword({ password_hash, reset_token }, () => Router.push('/'))
  }

  handleResetPassSubmit = values => {
    this.props.actions.resetPassword(values, () => Router.push('/'))
  }

  render() {
    const { loading, IsResetPassword } = this.props
    if (IsResetPassword)
      return <ResetPasswordForm loading={ loading } onSubmit={ this.handleResetPassSubmit } />

    return <CreatePasswordForm loading={ loading } onSubmit={ this.handleCreatePassSubmit } />
  }
}
