import React from 'react'
import { Form, Select, Drawer, Button } from 'antd'
import { DetailsCategory } from '../../../../components'
import { BottomBtnGroup } from './manage-add-technology.styles'
import PropTypes from 'prop-types'

const Option = Select.Option
const FormItem = Form.Item

@Form.create({})
export class ManageAddTechnology extends React.Component {
  static propTypes = {
    visible: PropTypes.bool.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
    technologyTypes: PropTypes.array.isRequired
  }

  handleSubmit = e => {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((errors, values) => {
      if (errors) return
      this.props.onSubmit(values)
    })
  }

  render() {
    const { visible, onClose, technologyTypes, loading } = this.props
    const { getFieldDecorator } = this.props.form
    const selectedType = this.props.form.getFieldValue('type')
    const technologies =
      !!selectedType &&
      technologyTypes.find(t => t.id === selectedType).technologies

    return (
      <Drawer
        width={500}
        visible={visible}
        placement='right'
        closable={true}
        maskClosable={false}
        onClose={onClose}
      >
        <DetailsCategory text='Add technology skill' main={true} />
        <Form layout='vertical' onSubmit={this.handleSubmit}>
          <FormItem label='Technology type'>
            {getFieldDecorator('type', {
              rules: [
                { required: true, message: 'Please select technology type' }
              ]
            })(
              <Select placeholder={'Select technology type'}>
                {this.props.technologyTypes.map(tech => (
                  <Option value={tech.id} key={tech.id}>
                    {tech.name}
                  </Option>
                ))}
              </Select>
            )}
          </FormItem>
          <FormItem label='Technology'>
            {getFieldDecorator('technologyId', {
              rules: [{ required: true, message: 'Please select technology' }]
            })(
              <Select
                placeholder={'Select technology type'}
                disabled={!selectedType}
              >
                {technologies &&
                  technologies.map(tech => (
                    <Option value={tech.id} key={tech.id}>
                      {tech.name}
                    </Option>
                  ))}
              </Select>
            )}
          </FormItem>
        </Form>
        <BottomBtnGroup>
          <Button
            style={{ marginRight: 8 }}
            onClick={onClose}
            disabled={loading}
          >
            Cancel
          </Button>
          <Button type='primary' onClick={this.handleSubmit} disabled={loading}>
            Ok
          </Button>
        </BottomBtnGroup>
      </Drawer>
    )
  }
}
