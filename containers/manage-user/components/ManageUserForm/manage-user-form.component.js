import React from 'react'
import { Button, Card, Checkbox, DatePicker, Form, InputNumber, Select, Input, Col, Row } from 'antd'
import PropTypes from 'prop-types'
import moment from 'moment'
import * as _ from 'lodash'

import { ActionsGroup } from './manage-user-form.styles'

const FormItem = Form.Item
const Option = Select.Option

const socialTypes = [
  { type: 'Facebook' },
  { type: 'YouTube' },
  { type: 'Instagram' },
  { type: 'Twitter' },
  { type: 'Reddit' },
  { type: 'Pinterest' },
  { type: 'Google+' },
  { type: 'LinkedIn' },
]

@Form.create({
  onValuesChange(props, changedValues, allValues) {
    let socials = [...socialTypes]
    if (!!allValues.social) {
      allValues.social.forEach((item, index) => {
        socials[index].url = !!item ? item.url : undefined
      })
    }
    console.log(mapPropsToFields, '------------')
    props.handleChangeForm({ ...allValues, social: [...socials] })
  },
  mapPropsToFields({ loading, isEdit, user, formData }) {
    if (loading) return
    const source = (Object.keys(user).length && !formData) ? user : formData
    if (source) {
      return {
        first_name: Form.createFormField({ value: source.first_name }),
        last_name: Form.createFormField({ value: source.last_name }),
        email: Form.createFormField({ value: source.email }),
        phone: Form.createFormField({ value: source.phone }),
        gender: Form.createFormField({ value: source.gender }),
        city: Form.createFormField({ value: source.city && source.city.id || source.city }),
        department: Form.createFormField({ value: source.department && source.department.id || source.department }),
        position: Form.createFormField({ value: source.position && source.position.id || source.position }),
        qualities: Form.createFormField({ value: source.qualities }),
        experience: Form.createFormField({ value: source.experience }),
        date_start_job: Form.createFormField({ value: source.date_start_job && moment(source.date_start_job) }),
        level: Form.createFormField({ value: source.level }),
        social: socialTypes.map(social => {
          const foundSocial = source.social && source.social.find(userSocial => social.type === userSocial.type)
          return ({ url: Form.createFormField({ value: foundSocial ? foundSocial.url : undefined }) })
        }),
        english_level: Form.createFormField({ value: source.english_level && source.english_level.id || source.english_level }),
        role: Form.createFormField({ value: source.role && source.role.id || source.role || 3 }),
      }
    }
  },
})
export class ManageUserForm extends React.Component {

  static propTypes = {
    cities: PropTypes.array.isRequired,
    departments: PropTypes.array.isRequired,
    positions: PropTypes.array.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    loading: PropTypes.bool,
    user: PropTypes.object,
    formData: PropTypes.object,
    handleChangeForm: PropTypes.func.isRequired,
    userRoles: PropTypes.array,
    isEdit: PropTypes.bool.isRequired,
    handleCancelClick: PropTypes.func.isRequired,
  }

  handleSubmit = e => {
    e.preventDefault()
    const { user, isEdit } = this.props

    this.props.form.validateFieldsAndScroll((errors, values) => {
      if (errors) return

      const newSocial = _.merge(values.social, socialTypes).map(item => {
        if (!!item.url && isEdit) {
          const foundSocial = user.social.find(social => social.type === item.type)
          if (foundSocial) return { ...foundSocial, url: item.url }
        }
        return item
      }).filter(social => !!social.url)

      const newValues = { ...values, social: newSocial }

      this.props.handleSubmit(newValues, !values.agreement, () => {
        if (!values.agreement) this.props.form.resetFields()
      })
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form
    const { loading, departments, cities, positions, handleCancelClick, englishLevels, userRoles } = this.props

    return (
      <Form onSubmit={ this.handleSubmit }>
        <Card title={ 'Personal information' }>
          <Row gutter={ 16 }>
            <Col span={ 6 }>
              <FormItem label={ 'First Name' }>
                { getFieldDecorator('first_name', {
                  rules: [{ required: true, message: 'Please input first name' }],
                })(<Input disabled={ loading } />) }
              </FormItem>
            </Col>
            <Col span={ 6 }>
              <FormItem label={ 'Last Name' }>
                { getFieldDecorator('last_name', {
                  rules: [{ required: true, message: 'Please input last name' }],
                })(<Input disabled={ loading } />) }
              </FormItem>
            </Col>
            <Col span={ 6 }>
              <FormItem label={ 'Email' }>
                { getFieldDecorator('email', {
                  rules: [
                    { required: true, message: 'Please input email' },
                    { type: 'email', message: 'The input is not valid E-mail!' },
                  ],
                })(<Input disabled={ loading } />) }
              </FormItem>
            </Col>
            <Col span={ 6 }>
              <FormItem label={ 'Phone' }>
                { getFieldDecorator('phone', {
                  rules: [
                    { required: true, message: 'Please input email' },
                  ],
                })(<Input style={ { width: '100%' } } disabled={ loading } />) }
              </FormItem>
            </Col>
          </Row>
          <Row gutter={ 16 }>
            <Col span={ 6 }>
              <FormItem label={ 'Gender' }>
                { getFieldDecorator('gender', {
                  rules: [{ required: true, message: 'Please select gender' }],
                })(
                  <Select disabled={ loading } placeholder={ 'Select gender' }>
                    <Option value={ 'MALE' }>Male</Option>
                    <Option value={ 'FEMALE' }>Female</Option>
                  </Select>)
                }
              </FormItem>
            </Col>
            <Col span={ 6 }>
              <FormItem label={ 'City' }>
                { getFieldDecorator('city', {
                  rules: [{ required: true, message: 'Please select city' }],
                })(
                  <Select disabled={ loading } placeholder={ 'Select city' }>
                    { cities
                      .map(city => <Option key={ city.id } value={ city.id }>{ city.name }</Option>) }
                  </Select>)
                }
              </FormItem>
            </Col>
            <Col span={ 6 }>
              <FormItem label={ 'Department' }>
                { getFieldDecorator('department', {
                  rules: [{ required: true, message: 'Please select department' }],
                })(
                  <Select disabled={ loading } placeholder={ 'Select department' }>
                    { departments
                      .map(dep => <Option key={ dep.id } value={ dep.id }>{ dep.name }</Option>) }
                  </Select>)
                }
              </FormItem>
            </Col>
            <Col span={ 6 }>
              <FormItem label={ 'Position' }>
                { getFieldDecorator('position', {
                  rules: [{ required: true, message: 'Please select position' }],
                })(
                  <Select disabled={ loading } placeholder={ 'Select position' }>
                    { positions
                      .map(pos => <Option key={ pos.id } value={ pos.id }>{ pos.name }</Option>) }
                  </Select>)
                }
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Input.Group>
              <FormItem label={ 'Features' }>
                { getFieldDecorator('qualities', {})(<Input.TextArea disabled={ loading } />) }
              </FormItem>
            </Input.Group>
          </Row>
        </Card>
        <Card title={ 'Technical information' }>
          <Row gutter={ 16 }>
            <Col span={ 6 }>
              <FormItem label={ 'Level of English' }>
                { getFieldDecorator('english_level', {
                  initialValue: 1,
                  rules: [{ required: true, message: 'Please select level of English' }],
                })(
                  <Select disabled={ loading } placeholder={ 'Select level of English' }>
                    { englishLevels.map(eL =>
                      <Option key={ eL.id } value={ eL.id }>{ eL.level }</Option>) }
                  </Select>)
                }
              </FormItem>
            </Col>
            <Col span={ 6 }>
              <FormItem label={ 'Experience (years)' }>
                { getFieldDecorator('experience', { initialValue: 1 })(
                  <InputNumber disabled={ loading } style={ { width: '100%' } } min={ 0 } max={ 30 } />) }
              </FormItem>
            </Col>
            <Col span={ 6 }>
              <FormItem label={ 'Work start date' }>
                { getFieldDecorator('date_start_job', {
                  rules: [{ required: true, message: 'Please select start work date' }],
                })(<DatePicker style={ { width: '100%' } } disabled={ loading } />) }
              </FormItem>
            </Col>
            <Col span={ 6 }>
              <FormItem label={ 'Level' }>
                { getFieldDecorator('level', {
                  initialValue: 1,
                  rules: [{ required: true, message: 'Please select level' }],
                })(
                  <Select disabled={ loading } placeholder={ 'Select level' }>
                    { new Array(10).fill(null).map((_, i) =>
                      <Option key={ i + 1 } value={ i + 1 }>{ i + 1 }</Option>) }
                  </Select>)
                }
              </FormItem>
            </Col>
          </Row>
        </Card>
        <Card title={ 'Social information' }>
          <Row gutter={ 16 }>
            { socialTypes.map((social, index) => (
              <Col span={ 6 } key={ social.type.toLowerCase() }>
                <FormItem label={ social.type }>
                  { getFieldDecorator(`social[${ index }].url`)(<Input disabled={ loading } />) }
                </FormItem>
              </Col>
            )) }
          </Row>
        </Card>
        <Card title={ 'Permissions' }>
          <Row gutter={ 16 }>
            <Col span={ 6 }>
              <FormItem label={ 'User role' }>
                { getFieldDecorator('role', { initialValue: 3 })(
                  <Select disabled={ loading } placeholder={ 'Select user role' }>
                    { userRoles.map(role => <Option key={ role.id } value={ role.id }>{ role.name }</Option>) }
                  </Select>)
                }
              </FormItem>
            </Col>
          </Row>
        </Card>
        <Card title={ 'Actions' }>
          <ActionsGroup compact={ true }>
            { !this.props.isEdit && <FormItem>
              { getFieldDecorator('agreement', {
                valuePropName: 'checked',
                initialValue: false,
              })(
                <Checkbox>Create more...</Checkbox>,
              ) }
            </FormItem> }
            <FormItem>
              <Button
                type={ 'danger' }
                disabled={ loading }
                onClick={ handleCancelClick }
              >
                { 'Cancel' }
              </Button>
            </FormItem>
            <FormItem>
              <Button
                type="primary"
                htmlType="submit"
                disabled={ loading }
              >
                { this.props.isEdit ? 'Update' : 'Create' }
              </Button>
            </FormItem>
          </ActionsGroup>
        </Card>
      </Form>
    )
  }
}
