import React from 'react'
import { Form, Modal, Select } from 'antd'
import PropTypes from 'prop-types'

const Option = Select.Option
const FormItem = Form.Item

@Form.create()
export class AddTechModal extends React.Component {

  static propTypes = {
    visible: PropTypes.bool.isRequired,
    handleOk: PropTypes.func.isRequired,
    handleCancel: PropTypes.func.isRequired,
    technologyTypes: PropTypes.array.isRequired,
  }

  render() {
    const { getFieldDecorator } = this.props.form
    const selectedType = this.props.form.getFieldValue('type')
    const selectedTech = this.props.form.getFieldValue('technologyId')
    const technologies = !!selectedType && this.props.technologyTypes.find(t => t.id === selectedType).technologies
    return (
      <Modal
        title="Add technology skill"
        visible={ this.props.visible }
        onOk={ this.props.handleOk }
        onCancel={ this.props.handleCancel }
      >
        <Form layout="vertical">
          <FormItem label="Technology type">
            { getFieldDecorator('type', {
              rules: [ { required: true, message: 'Please select technology type' } ],
            })(
              <Select placeholder={ 'Select technology type' }>
                { this.props.technologyTypes.map(tech => <Option value={ tech.id }
                                                                 key={ tech.id }>{ tech.name }</Option>) }
              </Select>)
            }
          </FormItem>
          <FormItem label="Technology">
            { getFieldDecorator('technologyId', {
              rules: [ { required: true, message: 'Please select technology' } ],
            })(
              <Select placeholder={ 'Select technology type' } disabled={ !selectedType }>
                { technologies && technologies.map(tech => <Option value={ tech.id }
                                                                   key={ tech.id }>{ tech.name }</Option>) }
              </Select>)
            }
          </FormItem>
        </Form>
      </Modal>
    )
  }
}