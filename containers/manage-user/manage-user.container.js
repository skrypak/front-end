import React from 'react'
import { Row, Col, Button, Card, Table } from 'antd'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Router from 'next/router'
import PropTypes from 'prop-types'

import {
  DepartmentActions,
  PositionActions,
  CityActions,
  UserActions,
  UserRolesActions,
  TechnologyTypeActions,
  TechnologyActions,
  EnglishLevelActions,
} from './../../actions'
import { ManageAddTechnology, ManageUserForm } from './components'

const { fetchAllDepartments } = DepartmentActions
const { fetchAllPositions } = PositionActions
const { fetchAllCities } = CityActions
const { createUser, fetchUserById, updateUser, clearManagedUser } = UserActions
const { fetchUserRoles } = UserRolesActions
const { fetchAllTechnologyTypes } = TechnologyTypeActions
const { fetchAllTechnologies } = TechnologyActions
const { fetchEnglishLevels } = EnglishLevelActions

@connect(
  state => ({
    positions: state.position.positions,
    cities: state.city.cities,
    departments: state.department.departments,
    technologyTypes: state.technologyType.technologyTypes,
    technologies: state.technology.technologies,
    user: state.user.managedUser,
    userRoles: state.userRoles.roles,
    englishLevels: state.englishLevel.levels,
  }),
  dispatch => ({
    actions: bindActionCreators(
      {
        fetchAllPositions,
        fetchAllCities,
        fetchAllDepartments,
        createUser,
        fetchAllTechnologyTypes,
        fetchUserById,
        updateUser,
        clearManagedUser,
        fetchUserRoles,
        fetchAllTechnologies,
        fetchEnglishLevels,
      },
      dispatch,
    ),
  }),
)
export class ManageUserContainer extends React.Component {
  static propTypes = {
    positions: PropTypes.arrayOf(PropTypes.object).isRequired,
    cities: PropTypes.arrayOf(PropTypes.object).isRequired,
    departments: PropTypes.arrayOf(PropTypes.object).isRequired,
    technologyTypes: PropTypes.arrayOf(PropTypes.object).isRequired,
    technologies: PropTypes.array,
    actions: PropTypes.shape({
      fetchAllPositions: PropTypes.func.isRequired,
      fetchAllCities: PropTypes.func.isRequired,
      fetchAllDepartments: PropTypes.func.isRequired,
      createUser: PropTypes.func.isRequired,
      fetchAllTechnologyTypes: PropTypes.func.isRequired,
      fetchUserById: PropTypes.func.isRequired,
      updateUser: PropTypes.func.isRequired,
      clearManagedUser: PropTypes.func.isRequired,
      fetchUserRoles: PropTypes.func.isRequired,
      fetchEnglishLevels: PropTypes.func.isRequired,
    }).isRequired,
    userId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    user: PropTypes.object,
  }

  constructor(props) {
    super(props)

    this.formRef = null
    this.state = {
      drawer: false,
      loading: false,
      userLoaded: false,
      technologies: [],
      done: false,
      formData: null,
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (state.done) return null

    const { user, userId, technologies: allTechnologies } = props

    if (user.technologies && userId && state.userLoaded) {
      const userTechnologyNames = user.technologies.map(tech => tech.name)
      const userTechnologies = allTechnologies.filter(tech =>
        userTechnologyNames.includes(tech.name),
      )

      const technologies = userTechnologies.map(({ id, technologyType }) => ({
        id,
        technologyId: id,
        type: technologyType.id,
      }))
      return { ...state, technologies, done: true }
    }
    return null
  }

  componentDidMount() {
    this.props.actions.fetchAllTechnologyTypes()
    this.props.actions.fetchAllTechnologies()
    this.props.actions.fetchAllPositions()
    this.props.actions.fetchAllCities()
    this.props.actions.fetchAllDepartments()
    this.props.actions.fetchEnglishLevels()
    this.props.actions.fetchUserRoles()

    if (this.props.userId) {
      this.props.actions.fetchUserById(this.props.userId, () => this.setState({ userLoaded: true }))
    } else {
      this.setState({ userLoaded: true })
    }
  }

  componentWillUnmount() {
    this.props.actions.clearManagedUser()
  }

  onLevelChange = (tech, v) =>
    this.setState({
      technologies: this.state.technologies.map(t => {
        if (tech === t.id) t.level = v
        return t
      }),
    })

  onRemoveTechnologyClick = tech => {
    this.setState({
      technologies: this.state.technologies.filter(t => t.id !== tech),
    })
  }

  generateColumns = () => {
    const { technologyTypes } = this.props
    return [
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        width: 80,
        render: (text, record) => {
          return technologyTypes
            .find(t => t.id === record.type)
            .technologies.find(t => t.id === record.technologyId).name
        },
      },
      {
        title: 'Type',
        dataIndex: 'type',
        key: 'type',
        width: 80,
        render: (text, record) => technologyTypes.find(t => t.id === record.type).name,
      },
      {
        title: 'Actions',
        key: 'actions',
        width: 100,
        render: (_, record) => (
          <span>
            <a href="javascript:;" onClick={ () => this.onRemoveTechnologyClick(record.id) }>
              Delete
            </a>
          </span>
        ),
      },
    ]
  }

  handleSubmit = (values, isRedirect, cb) => {
    this.setState({ loading: true })

    if (this.props.userId) {
      // @info: update user
      this.props.actions.updateUser(
        this.props.userId,
        {
          ...this.props.user,
          ...values,
          date_start_job: values.date_start_job.toISOString(),
          technologies: [...this.state.technologies],
          photo: '',
        },
        () => Router.push('/user'),
      )
    } else {
      // @info: create user
      this.props.actions.createUser(
        {
          ...values,
          date_start_job: values.date_start_job.toISOString(),
          password_hash: 'password',
          technologies: [...this.state.technologies],
        },
        () => {
          this.setState({ loading: false })
          if (isRedirect) Router.push('/user')
          if (cb instanceof Function) cb()
        },
      )
    }
  }

  saveFormRef = formRef => (this.formRef = formRef)

  onSubmit = values => {
    // @info: do not add the exist technology
    const tech = this.state.technologies.find(t => t.id === values.technologyId)
    if (!tech)
      this.setState({
        technologies: [...this.state.technologies, { id: values.technologyId, ...values }],
      })

    this.closeManageTechnology()
  }

  onCancelClick = () => Router.push('/user')

  openManageTechnology = () => this.setState({ drawer: true })

  closeManageTechnology = () => {
    this.formRef.props.form.resetFields()
    this.setState({ drawer: false })
  }

  onChangeForm = allFields => {
    this.setState({ formData: allFields })
  }

  render() {
    const { cities, departments, positions, technologyTypes, user, userId, englishLevels, userRoles } = this.props
    const { technologies, userLoaded, loading, drawer, formData } = this.state

    return (
      <React.Fragment>
        <Row>
          <Col span={ 16 }>
            <ManageUserForm
              cities={ cities }
              departments={ departments }
              positions={ positions }
              loading={ !userLoaded || loading }
              handleSubmit={ this.handleSubmit }
              handleCancelClick={ this.onCancelClick }
              user={ user }
              formData={ formData }
              handleChangeForm={ this.onChangeForm }
              userRoles={ userRoles }
              isEdit={ !!userId }
              englishLevels={ englishLevels }
            />
          </Col>
          <Col span={ 8 }>
            <Card
              title={ 'Skills' }
              extra={
                <a onClick={ this.openManageTechnology }>
                  Add skill
                </a>
              }
            >
              <Table
                size={ 'medium' }
                showHeader={ true }
                rowKey="id"
                pagination={ false }
                bordered={ false }
                hasData={ technologies.length > 0 }
                columns={ this.generateColumns() }
                dataSource={ technologies }
              />
            </Card>
          </Col>
        </Row>
        <ManageAddTechnology
          wrappedComponentRef={ this.saveFormRef }
          visible={ drawer }
          onSubmit={ this.onSubmit }
          onClose={ this.closeManageTechnology }
          technologyTypes={ technologyTypes }
          loading={ loading }
          technologies={ technologies }
        />
      </React.Fragment>
    )
  }
}
