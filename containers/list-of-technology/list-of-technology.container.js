import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Table, Divider, Pagination } from 'antd'
import PropTypes from 'prop-types'

import { TechnologyActions, TechnologyTypeActions } from './../../actions'
import { TableHeader } from '../../components'
import { constants } from '../../utils'
import { ManageTechnologiesComponent } from './components'
import { List } from '../../components/List/list.component'

const { createTechnology, updateTechnology, fetchAllTechnologies } = TechnologyActions;
const { fetchAllTechnologyTypes } = TechnologyTypeActions;

@connect(
  ({ technology: { technologies, loading }, technologyType: { technologyTypes } }) => ({
    technologies,
    loading,
    technologyTypes
  }),
  dispatch => ({
    actions: bindActionCreators(
      {
        createTechnology,
        updateTechnology,
        fetchAllTechnologies,
        fetchAllTechnologyTypes
      },
      dispatch
    )
  })
)
export class ListOfTechnology extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      filter: {
        orderBy: 'name',
        orderType: 'ASC',
        name: ''
      },
      managedTechnology: null,
      drawer: false,
      done: false
    };
  }

  static propTypes = {
    technologyId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    loading: PropTypes.bool.isRequired,
    actions: PropTypes.shape({
      updateTechnology: PropTypes.func.isRequired,
      fetchAllTechnologies: PropTypes.func.isRequired,
      createTechnology: PropTypes.func.isRequired
    }).isRequired
  };

  componentDidMount() {
    this.props.actions.fetchAllTechnologies(this.state.filter);
    this.props.actions.fetchAllTechnologyTypes();
  }

  onFilterChange = filter => {
    this.setState(
      {
        filter: {
          ...this.state.filter,
          ...filter
        }
      },
      () => this.props.actions.fetchAllTechnologies(this.state.filter)
    );
  };

  getOrderType = (c, orderType) => {
    if (!!c && Object.keys(c).length) {
      return constants.ORDER_TYPE[c.order];
    }
    if (orderType === constants.ORDER_TYPE.ascend) {
      return constants.ORDER_TYPE.descend;
    }
    return constants.ORDER_TYPE.ascend;
  };

  onOrderChange = (_, f, c) => {
    this.onFilterChange({
      orderBy: c.columnKey || 'name',
      orderType: this.getOrderType(c, this.state.filter.orderType)
    });
  };

  getTitle = () => (
    <TableHeader
      title="Technologies"
      href="/technology/create"
      buttonText="Create a new technology"
      customButtonHandler={ this.openManageTechnology }
      searchHandler={ value => this.onFilterChange({ name: value }) }
      searchPlaceholder="Type technology name"
    />
  );

  generateColumns() {
    return [
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        width: 150,
        sorter: true,
        sortOrder:
          this.state.filter.orderBy === 'name' &&
          constants.R_ORDER_TYPE[this.state.filter.orderType],
        render: (text, record) => <a href="javascript:;">{ `${record.name}` }</a>
      },
      {
        title: 'Actions',
        key: 'actions',
        width: 80,
        render: (_, record) => (
          <span>
            <a href="javascript:;" onClick={() => this.openManageTechnology(record.id)}>
              Edit
            </a>
            <Divider type="vertical" />
            <a href="javascript:;">Delete</a>
          </span>
        )
      }
    ];
  }

  openManageTechnology = id => {
    const managedTechnology = this.props.technologies.find(t => t.id === id);
    this.setState({ managedTechnology, drawer: true });
  };

  closeManageTechnology = () => this.setState({ managedTechnology: null, drawer: false });

  onSubmit = values => {
    if (this.state.managedTechnology) {
      this.props.actions.updateTechnology(
        { ...values, technologyId: this.state.managedTechnology.id },
        this.closeManageTechnology
      );
    } else {
      this.props.actions.createTechnology(values, this.closeManageTechnology);
    }
  };

  render() {
    const { technologies, loading } = this.props;
    return (
      <List>
        <Table
          size={ 'medium' }
          rowKey="id"
          title={ this.getTitle }
          showHeader={ true }
          pagination={ false }
          loading={ loading }
          hasData={ technologies.length > 0 }
          columns={ this.generateColumns() }
          dataSource={ technologies }
          onChange={ this.onOrderChange }
        />
        <ManageTechnologiesComponent
          visible={ this.state.drawer }
          onClose={ this.closeManageTechnology }
          onSubmit={ this.onSubmit }
          technology={ this.state.managedTechnology }
          technologyTypes={ this.props.technologyTypes }
          loading={ loading }
        />
      </List>
    );
  }
}
