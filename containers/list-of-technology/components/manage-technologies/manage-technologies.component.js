import React from 'react';
import PropTypes from 'prop-types';
import { Button, Col, Drawer, Form, Input, Select, Row } from 'antd';
import { DetailsCategory } from '../../../../components';
import { BottomBtnGroup } from './manage-technology.styles';

const CREATE_TEXT = 'Create a new technology';
const UPDATE_TEXT = 'Update technology';

@Form.create({
  mapPropsToFields({ technology }) {
    if (!!technology) {
      return {
        name: Form.createFormField({ value: technology.name }),
        technologyTypeId: Form.createFormField({ value: technology.technologyType.id })
      };
    }
    return {};
  }
})
export class ManageTechnologiesComponent extends React.Component {
  static propTypes = {
    visible: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    technology: PropTypes.object,
    loading: PropTypes.bool.isRequired
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((errors, values) => {
      if (errors) return;
      this.props.onSubmit(values);
    });
  };

  render() {
    const { visible, onClose, technology, loading, technologyTypes = [] } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <Drawer
        width={400}
        placement="right"
        closable={true}
        maskClosable={false}
        visible={visible}
        onClose={onClose}
      >
        <DetailsCategory text={technology ? UPDATE_TEXT : CREATE_TEXT} main={true} />
        <Form onSubmit={this.handleSubmit} layout="vertical">
          <Row>
            <Col>
              <Form.Item label={'Name'}>
                {getFieldDecorator('name', {
                  rules: [{ required: true, message: 'Please input technology' }]
                })(<Input disabled={loading} />)}
              </Form.Item>
            </Col>
            <Col>
              <Form.Item label={'Type'}>
                {getFieldDecorator('technologyTypeId', {
                  rules: [{ required: true, message: 'Please select technology type' }]
                })(
                  <Select disabled={loading} placeholder={'Select technology type'}>
                    {technologyTypes.map(type => (
                      <Select.Option key={type.id} value={type.id}>
                        {type.name}
                      </Select.Option>
                    ))}
                  </Select>
                )}
              </Form.Item>
            </Col>
          </Row>
        </Form>
        <BottomBtnGroup>
          <Button style={{ marginRight: 8 }} onClick={onClose} disabled={loading}>
            Cancel
          </Button>
          <Button type="primary" onClick={this.handleSubmit} disabled={loading}>
            Submit
          </Button>
        </BottomBtnGroup>
      </Drawer>
    );
  }
}
