import styled from 'styled-components'

export const TextInfoContainer = styled.div`
  min-width: 180px;
`

export const FieldText = styled.p`
  text-transform: uppercase;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 8px;
`

export const ValueText = styled.p`
  font-size: 16px;
  text-transform: capitalize;
  font-style: oblique;
`
