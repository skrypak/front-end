import React from 'react'
import { FieldText, ValueText, TextInfoContainer } from './text-info.styles'

export const TextInfo = props => (
  <TextInfoContainer>
    <FieldText>{ props.field }</FieldText>
    <ValueText>{ props.value }</ValueText>
  </TextInfoContainer>
)
