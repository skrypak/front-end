import React from 'react'
import PropTypes from 'prop-types'
import { Tag } from 'antd'
import { CaptionH4 } from '../../components/common'
import { technologyShape } from '../../constants/propTypes/user'

const TechologiesTagsList = ({technologies, withCaption, onTechnologyClick}) => (
  <React.Fragment>
    { withCaption && <CaptionH4> Technologies: </CaptionH4> }
    {
      technologies.length
        ? technologies.map( techology => 
            <Tag 
              key={techology.id} 
              onClick={onTechnologyClick(techology)}
            > 
              {techology.name}
           </Tag>
          )
        : <p> None </p>
    }
  </React.Fragment>
)

TechologiesTagsList.defaultProps = {
  withCaption: true,
  technologies: [],
  onTechnologyClick: (tag) => () => null
}

TechologiesTagsList.propTypes = {
  withCaption: PropTypes.bool,
  onTechnologyClick: PropTypes.func,
  technologies: PropTypes.arrayOf(
    PropTypes.shape(technologyShape)
  )
}

export default TechologiesTagsList