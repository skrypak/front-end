import React from 'react'
import PropTypes from 'prop-types'
import { MarginedIcon } from '../common'

export const UserMainInfo = ({email, phone, locationString}) => (
 <React.Fragment>
    <p>
      <MarginedIcon type='mail' />
      {email}
    </p>
    <p>
      <MarginedIcon type='phone' />
      {phone || 'none'}
    </p>
    <p>
      <MarginedIcon type='home' />
      { locationString }
    </p>
  </React.Fragment>
)

UserMainInfo.propTypes = {
  email: PropTypes.string,
  phone: PropTypes.string,
  locationString: PropTypes.string,
}

export default UserMainInfo
