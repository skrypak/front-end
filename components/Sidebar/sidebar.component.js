import { Layout, Menu, Icon } from 'antd'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import React from 'react'
import Link from 'next/link'

import { ConfigurationActions } from './../../actions'
import { ROLES } from './../../utils/config'
import { Logo, WrappedMenu } from './sidebar.styles'
import PropTypes from 'prop-types'

const MENU = [
  { title: 'DASHBOARD', url: '/dashboard', icon: 'dashboard', roles: [ROLES.ALL] },
  { title: 'PROJECTS', url: '/project', icon: 'solution', roles: [ROLES.ROOT, ROLES.ADMIN] },
  { title: 'CLIENTS', url: '/client', icon: 'team', roles: [ROLES.ROOT, ROLES.ADMIN] },
  { title: 'USERS', url: '/user', icon: 'user', roles: [ROLES.ROOT, ROLES.ADMIN] },
  { title: 'MY IDP', url: '/my-idp', icon: 'inbox', roles: [ROLES.USER] },
  { title: 'IDP', url: '/idp', icon: 'inbox', roles: [ROLES.ROOT, ROLES.ADMIN] },
  { title: 'CITY', url: '/city', icon: 'home', roles: [ROLES.ROOT, ROLES.ADMIN] },
  { title: 'DEPARTMENT', url: '/department', icon: 'pie-chart', roles: [ROLES.ROOT, ROLES.ADMIN] },
  { title: 'TECHNOLOGY_TYPE', url: '/technology-type', icon: 'appstore', roles: [ROLES.ROOT, ROLES.ADMIN] },
  { title: 'TECHNOLOGY', url: '/technology', icon: 'html5', roles: [ROLES.ROOT, ROLES.ADMIN] },
]

@connect(
  state => ({
    common: state.common,
    userId: state.user.currentUser.id,
    role: state.user.currentUser.role.name,
    configs: state.configuration.configs,
  }),
  dispatch => ({
    updateUserConfigurations: bindActionCreators(ConfigurationActions.updateUserConfigurations, dispatch),
  }),
)
export class Sidebar extends React.Component {
  static propTypes = {
    common: PropTypes.object.isRequired,
    userId: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
    role: PropTypes.string.isRequired,
    configs: PropTypes.object.isRequired,
    updateUserConfigurations: PropTypes.func.isRequired,
  }

  toggle = () => {
    const { userId, configs, updateUserConfigurations } = this.props
    updateUserConfigurations(userId, { sidebarCollapse: !configs.sidebarCollapse, user: userId, id: configs.id })
  }

  render() {
    const { currentUrl } = this.props.common
    const { sidebarCollapse } = this.props.configs
    return <Layout.Sider
      theme="light"
      collapsible
      trigger={ null }
      collapsed={ sidebarCollapse }
    >
      <Logo>
        <h3>LOGO</h3>
      </Logo>
      <WrappedMenu>
        <Menu theme="light" mode="inline" defaultSelectedKeys={ [currentUrl] }>
          { MENU
            .filter(m => m.roles.includes('*') || m.roles.includes(this.props.role))
            .map(m => (
              <Menu.Item key={ m.url }>
                <Link href={ m.url }>
                  <div>
                    <Icon type={ m.icon } />
                    <span>{ m.title }</span>
                  </div>
                </Link>
              </Menu.Item>
            )) }
        </Menu>
        <Menu selectable={ false } theme="light" mode="inline">
          <Menu.Item onClick={ this.toggle }>
            <Icon
              type={ sidebarCollapse ? 'menu-unfold' : 'menu-fold' }
              theme="outlined"
            />
            <span>Collapse sidebar</span>
          </Menu.Item>
        </Menu>
      </WrappedMenu>
    </Layout.Sider>
  }
}

