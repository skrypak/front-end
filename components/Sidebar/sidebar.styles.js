import styled from 'styled-components'

export const Logo = styled.div`
  height: 32px;
  background: #f0f2f5;
  margin: 16px;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`
export const WrappedMenu = styled.div`
  height: calc(100% - 64px);
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`

export const Logout = styled.div`
  height: 32px;
  cursor: pointer;
  padding-left: 24px;

  display: flex;
  flex-direction: row;
`
