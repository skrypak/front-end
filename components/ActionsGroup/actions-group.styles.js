import styled from 'styled-components'
import { Input } from 'antd'

export const ActionsGroup = styled(Input.Group)`
  display: flex!important;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
`