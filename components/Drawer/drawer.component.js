import React from 'react'
import { Drawer as AntDrawer, Button } from 'antd'
import { BottomBtnGroup } from './drawer.styles'

export const Drawer = ({
  children,
  disabled,
  width,
  visible,
  placement,
  onClose,
  onSubmit,
  closable = true,
  maskClosable = false,
}) => (
    <AntDrawer
      width={ width }
      visible={ visible }
      placement={ placement }
      closable={ closable }
      maskClosable={ maskClosable }
      onClose={ onClose }
    >
      { children }
      <BottomBtnGroup>
        <Button
          style={{ marginRight: 8 }}
          onClick={ onClose }
          disabled={ disabled }
        >
          Cancel
        </Button>
        <Button type="primary" onClick={ onSubmit } disabled={ disabled }>
          Ok
        </Button>
      </BottomBtnGroup>
    </AntDrawer>
  )
