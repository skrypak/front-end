import styled from 'styled-components'

export const Text = styled.p`
  font-size: 16px;
  color: rgba(0,0,0,0.85);
  line-height: 24px;
  display: block;
  margin-bottom: ${ props => props.main ? '24px' : '16px' };
  
`