import React from 'react'
import PropTypes from 'prop-types'
import {Text} from './details-category.styles'

export const DetailsCategory = props => (
  <Text main={props.main}>{ props.text }</Text>
)

DetailsCategory.propTypes = {
  text: PropTypes.string.isRequired,
  main: PropTypes.bool
}