import styled from 'styled-components'

export const Content = styled.div`
  font-size: 14px;
  color: rgba(0,0,0,0.65);
  line-height: 22px;
  margin-bottom: 7px;
`

export const TitleText = styled.p`
  margin-right: 8px;
  display: inline-block;
  color: rgba(0,0,0,0.85);
`