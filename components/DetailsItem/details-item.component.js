import React from 'react'
import { Content, TitleText } from './details-item.styles'

export const DetailsItem = ({ title, content }) => (
  <Content>
    <TitleText>
      { title }:
    </TitleText>
    { content }
  </Content>
)