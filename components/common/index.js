import styled from 'styled-components'
import { Icon } from 'antd'

export const CustomLink = styled.a`
  color: inherit;
  text-decoration: none;
  display: flex;
  align-items: center;  
  &:focus {
    text-decoration: none;
  }
`

export const AvatarCaption = styled.div`
  font-size: 20px;
  line-height: 28px;
  font-weight: 500;
  color: rgba(0,0,0,.85);
  padding: 1rem 0 0 0;
`

export const AvatarSubcaption = styled.div`
  font-size: 14px;
  color: rgba(0,0,0,.65);
  margin-bottom: 1rem;
`


export const MarginedIcon = styled(Icon)`
  margin-right: 0.5rem;
  font-size: 16px;
`

export const TextCenter = styled.div`
  text-align: center

`

export const Flex = styled.div`
  display: flex;
  align-items: center;
  ${props => props.cssString || ''}
`

export const CaptionH4 = styled.h4`
  margin-top: 0.7rem;
  margin-bottom: 0.5rem;
`
