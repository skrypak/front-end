
export const ORDER_TYPE = {
  ascend: 'ASC',
  descend: 'DESC',
}

export const R_ORDER_TYPE = {
  ASC: 'ascend',
  DESC: 'descend',
}