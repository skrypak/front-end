import moment from 'moment'

export const fullNameOf = (person, options = {}) => {
  const { 
    noPerson = 'No person',
    lastNameFirst = false
  } = options

  if(!person) return noPerson

  return lastNameFirst
    ? `${person.last_name} ${person.first_name}`
    : `${person.first_name} ${person.last_name}`
} 

export const getDate = (date, options = {}) => {
  const {
    format = 'YYYY-MM-DD',
    noDate = '...',
  } = options

  return date ? moment(date).format(format) : noDate
}
  