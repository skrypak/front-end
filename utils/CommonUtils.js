import Router from 'next/router'

export const redirectTo = (path, res) => {
  if (!process.browser) {
    res.writeHead(302, { Location: path })
    res.end()
  } else {
    Router.replace(path)
  }
}

