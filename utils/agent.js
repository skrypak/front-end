import axios from 'axios'
import Cookies from 'js-cookie'
import { API_URL } from './config'

const agent = axios.create({
  baseURL: API_URL
})

agent.interceptors.request.use(config => {
  if (process.browser) {
    const token = Cookies.get('token')
    config = { ...config, headers: { ...config.headers, 'Authorization': `Bearer ${token}` } }
  }
  return config
})

export { agent }
