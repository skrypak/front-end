import { createStore, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'
import rootReducer from './../reducers'
import { agent } from './agent'

const env = process.env.NODE_ENV

export const configureStore = (initialState) => {
  let middlewares = applyMiddleware(thunkMiddleware.withExtraArgument(agent))

  if (env !== 'production' && process.browser)
    middlewares = composeWithDevTools(
      middlewares,
      applyMiddleware(createLogger({ collapsed: true })),
    )
  return createStore(rootReducer, initialState, middlewares)
}
