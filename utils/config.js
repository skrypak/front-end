const env = process.env

export const API_URL = process.env.API_URL || 'http://stage.cms.company-name/api/v1'

export const ROLES = {
  ADMIN: 'ADMIN',
  ROOT: 'ROOT',
  USER: 'USER',
  ALL: '*',
}
