export const FILL_USER = '@crm-review/user/FILL_USER'
export const CLEAR_MANAGED_USER = '@crm-review/user/CLEAR_MANAGED_USER'

export const FILTER_USERS_REQUEST = '@crm-review/user/FILTER_USERS_REQUEST'
export const FILTER_USERS_SUCCESS = '@crm-review/user/FILTER_USERS_SUCCESS'
export const FILTER_USERS_ERROR = '@crm-review/user/FILTER_USERS_ERROR'

export const FETCH_ALL_USERS_REQUEST = '@crm-review/user/FETCH_ALL_USERS_REQUEST'
export const FETCH_ALL_USERS_SUCCESS = '@crm-review/user/FETCH_ALL_USERS_SUCCESS'
export const FETCH_ALL_USERS_ERROR = '@crm-review/user/FETCH_ALL_USERS_ERROR'

export const FETCH_USERS_BY_POSITION_REQUEST = '@crm-review/user/FETCH_USERS_BY_POSITION_REQUEST'
export const FETCH_USERS_BY_POSITION_SUCCESS = '@crm-review/user/FETCH_USERS_BY_POSITION_SUCCESS'
export const FETCH_USERS_BY_POSITION_ERROR = '@crm-review/user/FETCH_USERS_BY_POSITION_ERROR'

export const CREATE_USER_REQUEST = '@crm-review/user/CREATE_USER_REQUEST'
export const CREATE_USER_SUCCESS = '@crm-review/user/CREATE_USER_SUCCESS'
export const CREATE_USER_ERROR = '@crm-review/user/CREATE_USER_ERROR'

export const FETCH_USER_REQUEST = '@crm-review/user/FETCH_USER_REQUEST'
export const FETCH_USER_SUCCESS = '@crm-review/user/FETCH_USER_SUCCESS'
export const FETCH_USER_ERROR = '@crm-review/user/FETCH_USER_ERROR'

export const UPDATE_USER_REQUEST = '@crm-review/user/UPDATE_USER_REQUEST'
export const UPDATE_USER_SUCCESS = '@crm-review/user/UPDATE_USER_SUCCESS'
export const UPDATE_USER_ERROR = '@crm-review/user/UPDATE_USER_ERROR'
