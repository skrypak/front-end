export const FETCH_USER_ROLES_REQUEST = '@crm-review/user-roles/FETCH_USER_ROLES_REQUEST'
export const FETCH_USER_ROLES_ERROR = '@crm-review/user-roles/FETCH_USER_ROLES_ERROR'
export const FETCH_USER_ROLES_SUCCESS = '@crm-review/user-roles/FETCH_USER_ROLES_SUCCESS'
