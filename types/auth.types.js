export const LOGIN_REQUEST = '@crm-review/auth/LOGIN_REQUEST'
export const LOGIN_ERROR = '@crm-review/auth/LOGIN_ERROR'
export const LOGIN_SUCCESS = '@crm-review/auth/LOGIN_SUCCESS'

export const LOGOUT = '@crm-review/auth/LOGOUT'

export const RESET_PASS_REQUEST = '@crm-review/auth/RESET_PASS_REQUEST'
export const RESET_PASS_SUCCESS = '@crm-review/auth/RESET_PASS_SUCCESS'
export const RESET_PASS_ERROR = '@crm-review/auth/RESET_PASS_ERROR'

export const CREATE_PASS_REQUEST = '@crm-review/auth/CREATE_PASS_REQUEST'
export const CREATE_PASS_SUCCESS = '@crm-review/auth/CREATE_PASS_SUCCESS'
export const CREATE_PASS_ERROR = '@crm-review/auth/CREATE_PASS_ERROR'
