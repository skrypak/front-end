import React, { Component } from 'react'
import { Layout } from 'antd'
import { parseCookies, destroyCookie } from 'nookies'
import axios from 'axios'

import { Header, Sidebar, CheckAuthLoader } from './../components'
import { GlobalStyles, CommonUtils, config } from './../utils'
import { CommonActions } from './../actions'
import { COMMON_TYPES, CONFIGURATION_TYPES, USER_TYPES } from './../types'
import { LayoutStyles } from './../styles'

export const Page = (props = {}) => Component => {
  return class extends Component {
    static async getInitialProps(ctx) {
      let initialProps = {}
      const { store, asPath, isServer } = ctx

      /**
       * Checking on the server side user auth before access him to the page
       */
      if (isServer && props.auth) {
        const { token } = parseCookies(ctx)
        const configHeader = {
          headers: { 'Authorization': `Bearer ${ token }` },
        }
        try {
          const { data } = await axios.get(`${ config.API_URL }/user/me`, configHeader)
          store.dispatch({ type: USER_TYPES.FILL_USER, payload: data })
          initialProps = { ...initialProps, role: data.role }
          const { data: payload } = await axios.get(`${ config.API_URL }/user-config/${ data.id }`, configHeader)
          store.dispatch({ type: CONFIGURATION_TYPES.FETCH_CONFIGURATIONS_SUCCESS, payload: payload })
        } catch (e) {
          store.dispatch({ type: COMMON_TYPES.CLEAR_STORE })
          destroyCookie(ctx, 'token')
          CommonUtils.redirectTo('/', ctx.res)
        }
      } else if (!isServer && props.auth) {
        /** PUSH CURRENT USER ROLE IF PAGE RENDERS ON THE CLIENT SIDE */
        const { role } = store.getState().user.currentUser
        initialProps = { ...initialProps, role }
      }

      if (Component.getInitialProps) {
        const pr = await Component.getInitialProps(ctx)
        initialProps = { ...initialProps, ...pr }
      }
      const pattern = /^\/\w+(-\w+)|^\/\w+/
      store.dispatch(CommonActions.setCurrentUrl(((pattern.test(asPath) && asPath.match(pattern)[0]) || '')))
      return { ...initialProps }
    }

    constructor(props) {
      super(props)
      this.state = { done: false }
    }

    componentDidMount() {
      this.setState({ done: true })
    }

    render() {
      const layout = [
        <GlobalStyles key="global-styles" />,
      ]
      if (!this.state.done) return [
        ...layout,
        <CheckAuthLoader key='check auth loader' />,
      ]
      return [
        ...layout,
        <Layout key="pagefragment" style={ { height: '100%' } }>
          { props.header && <Sidebar /> }
          <Layout.Content>
            { props.sidebar && <Header /> }
            <LayoutStyles.Layout>
              <Component key="page" { ...this.props } />
            </LayoutStyles.Layout>
          </Layout.Content>
        </Layout>,
      ]
    }
  }
}
