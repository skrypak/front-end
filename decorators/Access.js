import React, { Component } from 'react'

import { ROLES } from './../utils/config'
import { CommonUtils } from './../utils'

export const Access = (access = []) => Component => {
  return class extends Component {

    static async getInitialProps(ctx) {
      let initialProps = {}
      const { role = {} } = ctx.store.getState().user.currentUser

      if (!access.includes(ROLES.ALL) && !access.includes(role.name)) {
        CommonUtils.redirectTo('/access-denied', ctx.res)
      }
      if (Component.getInitialProps) initialProps = await Component.getInitialProps(ctx)

      return { ...initialProps }
    }

    render() {
      return <Component key="page" { ...this.props } />
    }
  }
}
